![OSS Review Toolkit Logo](https://raw.githubusercontent.com/oss-review-toolkit/ort/main/logos/ort.png)
&nbsp;

[![Slack][1]][2] [![License][3]][4] [![REUSE status][5]][6]

[OSS Review Toolkit][7] (ORT) provides tooling to safely use, integrate, modify and redistribute third party software including [FOSS][8]. 

You can use it to:
- Automate your FOSS policy using _Policy as Code_ to do licensing, security vulnerabilities and engineering standards checks for your software project and its dependencies
- Generate [CycloneDX][9] or [SPDX][10] SBOMs for your software project
- Create a source code archive for your software project, including its dependencies

# Want to Help or have Questions?

All contributions are welcome. If you are interested in contributing, please read our
[contributing guide](https://github.com/oss-review-toolkit/.github/blob/main/CONTRIBUTING.md), and to get quick answers
to any of your questions we recommend you [join our Slack community](https://join.slack.com/t/ort-talk/shared_invite/enQtMzk3MDU5Njk0Njc1LThiNmJmMjc5YWUxZTU4OGI5NmY3YTFlZWM5YTliZmY5ODc0MGMyOWIwYmRiZWFmNGMzOWY2NzVhYTI0NTJkNmY).

# License

Copyright (C) 2017-2022 HERE Europe B.V.\
Copyright (C) 2019-2020 Bosch Software Innovations GmbH\
Copyright (C) 2020-2022 Bosch.IO GmbH

See the [LICENSE](./LICENSE) file in the root of this project for license details.

OSS Review Toolkit (ORT) is a [Linux Foundation project](https://www.linuxfoundation.org) and part of [ACT](https://automatecompliance.org/).

[1]: https://img.shields.io/badge/Join_us_on_Slack!-ort--talk-blue.svg?longCache=true&logo=slack
[2]: https://join.slack.com/t/ort-talk/shared_invite/enQtMzk3MDU5Njk0Njc1LThiNmJmMjc5YWUxZTU4OGI5NmY3YTFlZWM5YTliZmY5ODc0MGMyOWIwYmRiZWFmNGMzOWY2NzVhYTI0NTJkNmY
[3]: https://img.shields.io/badge/License-Apache%202.0-blue.svg
[4]: http://www.apache.org/licenses/LICENSE-2.0
[5]: https://api.reuse.software/badge/github.com/oss-review-toolkit/ort
[6]: https://api.reuse.software/info/github.com/oss-review-toolkit/ort
[7]: https://github.com/oss-review-toolkit/ort
[8]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[9]: https://cyclonedx.org
[10]: https://spdx.dev